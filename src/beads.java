/*
ID: nachoro1
LANG: JAVA
TASK: test
*/

import java.io.*;
import java.util.*;

class beads {

    public static void main (String [] args) throws IOException {
        BufferedReader f = new BufferedReader(new FileReader("test.in"));
        PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("test.out")));
        int numCuentas=Integer.parseInt(f.readLine());
        String cuentas=f.readLine();
        //rbw rojo-azul-blanco
        //wwwbbrwrbrbrrbrbrwrwwrbwrwrrb
        String corte;
        int max=0;
        int contD=0, contI=0, suma=0;
        int tam=cuentas.length();
        String letraAnterior=cuentas.charAt(tam-1)+"";
        for (int i=0; i<=cuentas.length();i++) {
            corte=cuentas.charAt(i)+"";
            if (corte.equals("b") || corte.equals("r")) {
                contD=cuentaDerecha(i,cuentas,corte);
                contI=cuentaIzquierda(i, cuentas, corte);
                suma=contI+contD;
                if (suma>max) {max=suma;}
            } else {
                //Pruebo primero como b
                contD=cuentaDerecha(i,cuentas,"b");
                contI=cuentaIzquierda(i, cuentas, "b");
                suma=contI+contD;
                if (suma>max) {max=suma;}
                //Y ahora como r
                contD=cuentaDerecha(i,cuentas,"r");
                contI=cuentaIzquierda(i, cuentas, "r");
                suma=contI+contD;                
                if (suma>max) {max=suma;}
            }
        }
        out.println(max);
        out.close();                                  
        System.exit(0);                               
    }

    static int cuentaIzquierda(int pos, String cuentas, String car) {
      return 0;
    }

    static int cuentaDerecha(int pos, String cuentas, String car) {
      return 0;
    }
}
    
