/*
ID: nachoro1
LANG: JAVA
TASK: gift1
*/

import java.io.*;
import java.util.*;

class gift1 {

  public static void main (String [] args) throws IOException {
    int numPersonas, cantidadDonada, personasDono;
    int eurosXPersona, sobrante, pos;
    String nombre, linea;
    BufferedReader f = new BufferedReader(new FileReader("gift1.in"));
    PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("gift1.out")));
    StringTokenizer st;
    
    // Leemos el numero de personas
    numPersonas=Integer.parseInt(f.readLine());
    
    // Creo los dos arrays que necesito
    String personas[]=new String[numPersonas];
    int euros[]=new int[numPersonas];
    
    // Relleno el array personas:
    for (int i=0; i<numPersonas; i++) {
        personas[i]=f.readLine();
    }
    
    for (int i=0; i<numPersonas; i++) {
        nombre=f.readLine();
        st=new StringTokenizer(f.readLine());
        cantidadDonada=Integer.parseInt(st.nextToken());
        personasDono=Integer.parseInt(st.nextToken());
        if (personasDono!=0) {
            eurosXPersona=cantidadDonada/personasDono;
            sobrante=cantidadDonada%personasDono;
            pos=buscaNombre(nombre, personas);
            euros[pos]+=sobrante-cantidadDonada;
            for (int j=0; j<personasDono; j++) {
                nombre=f.readLine();
                pos=buscaNombre(nombre, personas);
                euros[pos]+=eurosXPersona;
            }
        } 
    }
    
    for (int i=0; i<numPersonas; i++) {
        out.println(personas[i]+ " " + euros[i]);
    }
    out.close();                                  
    System.exit(0);                               
  }
  
  static int buscaNombre(String nombre, String[] nombres) {
      int pos=0;
      for (int i=0; i<nombres.length; i++) {
          if (nombres[i].equals(nombre)) {
              pos=i;
              break;
          }
      }
      return pos;
  }
}
    