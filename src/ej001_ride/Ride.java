/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ej001_ride;

/*
ID: nachoro1
LANG: JAVA
TASK: ride
*/

import java.io.*;
import java.util.*;

class Ride {
    public static void main (String [] args) throws IOException {
        StringTokenizer st;
        String cometa, grupo;
        int sumCometa, sumGrupo;
        BufferedReader f = new BufferedReader(new FileReader("ride.in"));
        PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("ride.out")));
        cometa=f.readLine();
        grupo=f.readLine();
        sumCometa=calculaResumen(cometa);
        sumGrupo=calculaResumen(grupo);
        if (sumCometa%47==sumGrupo%47) {
            out.println("GO");
        } else {
            out.println("STAY");
        }
        out.close();                                  
        System.exit(0);                               
    }
    
    static int calculaResumen(String cadena) {
        int resumen=1;
        int car;
        for (int i=0; i<cadena.length(); i++) {
            car=cadena.charAt(i)-64; //AB
            resumen*=car;
        }
        return resumen;
    }
}
    
