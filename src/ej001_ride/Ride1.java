/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ej001_ride;

import java.io.*;
import java.util.*;

/*
ID: nachoro1
LANG: JAVA
TASK: ride
*/
public class Ride1 {
    public static void main (String [] args) throws IOException {
        StringTokenizer st;
        String cometa, grupo;
        int sumCometa, sumGrupo;
        BufferedReader f = new BufferedReader(new FileReader("ride.in"));
        PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("ride.out")));
        String linea=f.readLine();
        while(linea!=null) {
            st = new StringTokenizer(linea);
            cometa=st.nextToken();
            linea=f.readLine();
            st = new StringTokenizer(linea);
            grupo=st.nextToken();
            sumCometa=calculaResumen(cometa);
            sumGrupo=calculaResumen(grupo);
            if (sumCometa%47==sumGrupo%47) {
                out.println("GO");
            } else {
                out.println("STAY");
            }
            linea=f.readLine();
        }

        /*int i1 = Integer.parseInt(st.nextToken());  
        int i2 = Integer.parseInt(st.nextToken());    
        out.println(i1+i2);*/
        out.close();                                  
        System.exit(0);                               
    }
    
    static int calculaResumen(String cadena) {
        int resumen=1;
        int car;
        for (int i=0; i<cadena.length(); i++) {
            car=cadena.charAt(i)-64;
            resumen*=car;
        }
        return resumen;
    }
}
    
