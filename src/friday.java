/*
ID: nachoro1
LANG: JAVA
TASK: friday
*/
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

class friday {
    public static void main(String[] args) throws IOException {
        BufferedReader f = new BufferedReader(new FileReader("friday.in"));
        PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("friday.out")));
        int n=Integer.parseInt(f.readLine());
        String dias="sdlmxjv";
        String dia1="l";
        String dia13;
        String respuesta="";
        int frecuencia[]=new int[7]; //0-s 1-d ... 6-v
        int diasMesActual;
        for (int anyo=1900; anyo<1900+n; anyo++) {
            for (int mes=1; mes<=12; mes++) {
                diasMesActual=devuelveDiasMes(mes, esBisiesto(anyo));
                dia13=devuelveDiaSemana(dia1,13);
                int pos=dias.indexOf(dia13);
                frecuencia[pos]+=1;
                dia1=devuelveDiaSemana(dia1,diasMesActual+1);
            }
        }
        respuesta=frecuencia[0]+"";
        for (int i=1;i<frecuencia.length;i++) {
            respuesta+=" "+frecuencia[i];
        }
        respuesta+="\n";
        out.print(respuesta);
        out.close();
        System.exit(0);
    }
    
    static String devuelveDiaSemana(String dia, int numDia) {
        String dias="sdlmxjv"; //dia="l" numDia=13 32
        int pos=dias.indexOf(dia); //pos=2
        pos+=numDia-1; //14 33
        pos=pos%7; //0="s" 5="j"
        return dias.charAt(pos)+"";
    }    
    
    static int devuelveDiasMes(int mes, boolean esBisiesto) {
        int dias=0;
        switch (mes) {
            case 1:
            case 3:
            case 5:
            case 7:
            case 8:
            case 10:
            case 12:
                dias=31;
                break;
            case 4:
            case 6:
            case 9:
            case 11:
                dias=30;
                break;
            case 2:
                if (esBisiesto) {
                    dias=29;
                } else {
                    dias=28;
                } 
                break;
        }
        return dias;
    }
    
    static boolean esBisiesto(int anyo) {
        boolean esBisiesto=false;
        if (anyo%4==0) {
            esBisiesto=true;
            if (anyo%100==0) {
                esBisiesto=false;
                if (anyo%400==0) {
                    esBisiesto=true;
                }
            }
        }
        return esBisiesto;
    }
}

